import java.util.*;

class Cache
{
	KeyValue[] keyvalue;
	Object[] list;
	Counter[] counters;

	int kvc; // key-value count
	int lc; // list elements count
	int cc; // counters count

	Cache()
	{
		keyvalue=new KeyValue[2];
		list=new Object[2];
		counters=new Counter[2];
		kvc=0;
		lc=0;
		cc=0;
	}

	// To insert key value
	public void insert_key_value(String key,String value,int ttl)
	{
		// If key-value array is full
		if(this.keyvalue.length-1 == this.kvc)
		{
			// Create new object with size 10 more than size of existing object
			KeyValue[] newkeyvalue= new KeyValue[this.kvc+10];
			// Add old elements 
			for(int i=0; i<keyvalue.length;i++)
				newkeyvalue[i]=keyvalue[i];
			// Assign this new object to old one
			this.keyvalue=newkeyvalue;
			// Release new object
			newkeyvalue=null;
		}

		// Insert into key-value array
		keyvalue[kvc]=new KeyValue(key,value,ttl);
		// Increment key-value object count
		this.kvc+=1;

		// Timer object
		Timer timer= new Timer();

		// Schedule the task for given ttl
		timer.schedule(new TimerTask() { public void run() {
   		System.out.println("timer working"); remove_key_value(key); timer.cancel(); timer.purge();  
   		}  },ttl*1000); 
	}

	// To delete key
	public void remove_key_value(String key1)
	{
		// Flag to indicate key found
		int flag=0;

		for(int i=0;i<this.kvc;i++)
		{
			// If matching key found, turn on the flag
			if(keyvalue[i].key.equals(key1))
				flag=1;
			// If match was found, shift elements
			if(flag==1 && i < kvc-1)
				keyvalue[i]=keyvalue[i+1];
			// Deallocate last object
			if(flag==1 && i==kvc-1)
				keyvalue[i]=null;
		}
		// Decrease key-value object count
		this.kvc-=1;
		System.out.println("Removed "+key1);
	}

	// Inserting Objects into list
	public void insert_list(Object o)
	{
		if(this.list.length-1 == this.lc)
		{
			// Create new object with size 10 more than size of existing object
			Object[] newList= new Object[this.lc+10];
			// Add old elements 
			for(int i=0; i<list.length;i++)
				newList[i]=list[i];
			// Assign this new object to old one
			this.list=newList;
			// Release new object
			newList=null;
		}

		// Insert into key-value array
		list[lc]=new Object();
		list[lc]=o;

		System.out.println(list[lc]);

		// Increment key-value object count
		this.lc+=1;
	}

	// Deleting Objects from list
	public void remove_list(Object o)
	{
		// Flag to indicate object found
		int flag=0;

		for(int i=0;i<this.lc;i++)
		{
			// If matching key found, turn on the flag
			if(list[i].equals(o))
				flag=1;
			// If match was found, shift elements
			if(flag==1 && i < lc-1)
				list[i]=list[i+1];
			// Deallocate last object
			if(flag==1 && i==lc-1)
				list[i]=null;
		}
		// Decrease key-value object count
		this.lc-=1;
		System.out.println("Removed "+o);	
	}

	// Adding a counter
	public void insert_counter(String name1)
	{
		// Check if the name already exists
		for(int i=0;i<this.cc;i++)
			if(this.counters[i].name.equals(name1))
			{
				System.out.println("Name already exists");
				return;
			}

		if(this.counters.length-1 == this.cc)
		{
			// Create new object with size 10 more than size of existing object
			Counter[] newcounter= new Counter[this.lc+10];
			// Add old elements 
			for(int i=0; i<counters.length;i++)
				newcounter[i]=counters[i];
			// Assign this new object to old one
			this.counters=newcounter;
			// Release new object
			newcounter=null;
		}

		// Insert into key-value array
		counters[cc]=new Counter(name1);

		// Increment key-value object count
		this.cc+=1;

	}

	// Increment the counter
	public void inc_counter(String name1,int value)
	{
		// Find the counter and increment
		for(int i=0;i<this.cc;i++)
			if(this.counters[i].name.equals(name1))
			{
				counters[i].count+=value;
				return;
			}
		System.out.println("Counter with this name not present");
	}

	// Decrement counter value
	public void dec_counter(String name1,int value)
	{
		// Find the counter and increment
		for(int i=0;i<this.cc;i++)
			if(this.counters[i].name.equals(name1))
			{
				counters[i].count-=value;
				return;
			}
		System.out.println("Counter with this name not present");	
	}
}