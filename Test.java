class Test
{

	public static void main(String args[])
	{
		Cache c= new Cache();

		c.insert_key_value("abcd","123",2);
		c.insert_key_value("efgh","123",5);
		c.insert_key_value("pqrs","123",10);

		c.insert_list("xyz");
		c.insert_list(100);
		c.insert_list(23.5);
		System.out.println(c.lc);
		c.remove_list("xyz");

		c.insert_counter("abc");
		System.out.println(c.counters[0].name+c.counters[0].count);
		c.inc_counter("abc",5);
		System.out.println(c.counters[0].name+c.counters[0].count);
		c.dec_counter("abc",2);
		System.out.println(c.counters[0].name+c.counters[0].count);
	}

}